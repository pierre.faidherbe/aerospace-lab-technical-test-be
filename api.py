from fastapi import FastAPI, Request, WebSocket
from pydantic import BaseModel

from database import (
    get_geojson,
    get_geojson_names,
    get_last_position,
    get_last_sun_timeframe,
    write_in_table_geojson,
)
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/iss/sun")
def iss_sun(
    limit: int = 10,
):
    return get_last_sun_timeframe(limit)


@app.get("/iss/position")
def iss_position():
    return get_last_position()


class Geojson(BaseModel):
    type: str
    features: list


@app.post("/geojson/{name}")
def geojson(name: str, geojson: Geojson):
    return write_in_table_geojson(name, geojson.model_dump_json())


@app.get("/geojson")
def geojson(name: str):
    return get_geojson(name)


@app.get("/geojson/name")
def geojson_name():
    return get_geojson_names()
