import queue
import threading
import time
from typing import Callable

import requests
from database import write_in_table_log

FETCH_INTERVAL = 20


def fetch_thread(
    url="https://api.wheretheiss.at/v1/satellites/25544", interval=FETCH_INTERVAL
):
    while True:
        response = requests.get(url)
        if response.status_code == 200:
            print("Request successful")
            data = response.json()
            yield data

        else:
            print(f"Request failed with status code: {response.status_code}")
        time.sleep(interval)  # Wait for 20 seconds


class FetchWorker(threading.Thread):
    def __init__(self, queue: queue.Queue, generator: Callable):
        super().__init__()
        self.queue = queue
        self.generator = generator

    def run(self):
        for data in self.generator():
            self.queue.put(data)


class DatabaseLogWorker(threading.Thread):
    def __init__(self, queue: queue.Queue):
        super().__init__()
        self.queue = queue

    def run(self):
        while True:
            data = self.queue.get()
            write_in_table_log(data)
            self.queue.task_done()


def start_iss_fetch_worker():
    data_queue = queue.Queue()

    database_worker = DatabaseLogWorker(data_queue)
    database_worker.start()

    fetch_worker = FetchWorker(data_queue, fetch_thread)
    fetch_worker.start()
