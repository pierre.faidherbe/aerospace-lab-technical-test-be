from database import create_database
from worker import start_iss_fetch_worker


def main():
    create_database()
    start_iss_fetch_worker()
    print("Main program continues")


if __name__ == "__main__":
    main()
