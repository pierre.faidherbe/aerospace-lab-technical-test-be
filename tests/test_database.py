import unittest
import sqlite3

from database import get_last_sun_timeframe

DATABASE_NAME = "iss.sqlite"
TABLE_NAME = "iss_log"


class TestDatabase(unittest.TestCase):
    def test_get_last_sun_timeframe(self):
        conn = sqlite3.connect(DATABASE_NAME)
        cursor = conn.cursor()
        total = 0
        cursor.execute(
            f"SELECT COUNT(*) FROM {TABLE_NAME} WHERE visibility = 'daylight'"
        )
        total_daylight_count = cursor.fetchone()[0]

        res = get_last_sun_timeframe(10)

        for row in res:
            start_time = row["start_time"]
            end_time = row["end_time"]

            assert start_time < end_time

            sql_query = f"""select count (*) from iss_log 
            where visibility = 'daylight'
            and timestamp BETWEEN '{start_time}' AND '{end_time}'"""

            cursor.execute(sql_query)

            daylight_count = cursor.fetchone()[0]

            total += daylight_count

        self.assertEqual(total, total_daylight_count)
