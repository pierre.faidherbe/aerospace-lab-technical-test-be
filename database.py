import sqlite3


DATABASE_NAME = "iss.sqlite"
TABLE_LOG_NAME = "iss_log"
TABLE_GEOJSON_NAME = "geojson"


def create_database():
    conn = sqlite3.connect(DATABASE_NAME)
    cursor = conn.cursor()
    cursor.execute(
        f"CREATE TABLE IF NOT EXISTS {TABLE_LOG_NAME} (timestamp INTEGER PRIMARY KEY, latitude REAL, longitude REAL, altitude REAL, velocity REAL, visibility TEXT, footprint INTEGER, daynum INTEGER, solar_lat REAL, solar_lon REAL)"
    )
    cursor.execute(
        f"CREATE TABLE IF NOT EXISTS {TABLE_GEOJSON_NAME} (name TEXT PRIMARY KEY, geojson TEXT)"
    )

    conn.commit()
    conn.close()


def write_in_table_log(data):
    conn = sqlite3.connect(DATABASE_NAME)
    cursor = conn.cursor()
    cursor.execute(
        f"INSERT INTO {TABLE_LOG_NAME} (timestamp, latitude, longitude, altitude, velocity, visibility, footprint, daynum, solar_lat, solar_lon) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
        (
            data["timestamp"],
            data["latitude"],
            data["longitude"],
            data["altitude"],
            data["velocity"],
            data["visibility"],
            data["footprint"],
            data["daynum"],
            data["solar_lat"],
            data["solar_lon"],
        ),
    )
    conn.commit()
    conn.close()


def write_in_table_geojson(name: str, geojson: str):
    conn = sqlite3.connect(DATABASE_NAME)
    cursor = conn.cursor()
    cursor.execute(
        f"INSERT INTO {TABLE_GEOJSON_NAME} (name, geojson) VALUES (?, ?)",
        (
            name,
            geojson,
        ),
    )
    conn.commit()
    conn.close()


def get_geojson(name):
    conn = sqlite3.connect(DATABASE_NAME)
    cursor = conn.cursor()
    cursor.execute(
        f"SELECT geojson FROM {TABLE_GEOJSON_NAME} WHERE name = ?",
        (name,),
    )
    row = cursor.fetchone()
    conn.close()

    return row[0]


def get_geojson_names():
    conn = sqlite3.connect(DATABASE_NAME)
    cursor = conn.cursor()
    cursor.execute(
        f"SELECT name FROM {TABLE_GEOJSON_NAME}",
    )
    row = cursor.fetchall()

    flattened_list = [y for x in row for y in x]

    conn.close()

    return flattened_list


def get_last_position():
    conn = sqlite3.connect(DATABASE_NAME)
    conn.row_factory = dict_factory
    cursor = conn.cursor()
    cursor.execute(f"SELECT * FROM {TABLE_LOG_NAME} ORDER BY timestamp DESC LIMIT 1")
    row = cursor.fetchone()
    conn.close()

    return row


def get_last_sun_timeframe(limit: int):
    conn = sqlite3.connect(DATABASE_NAME)
    conn.row_factory = dict_factory
    cursor = conn.cursor()
    cursor.execute(
        f"""
        WITH Sequences AS (
        SELECT 
            visibility,
            timestamp,
            LAG (visibility) OVER (ORDER BY timestamp) AS prev_visibility,
            LEAD(visibility) OVER (ORDER BY timestamp) AS next_visibility
        FROM 
            {TABLE_LOG_NAME}
        ),
        DaylightStart AS (
        SELECT 
            timestamp AS start_time
        FROM 
            Sequences
        WHERE 
            visibility = 'daylight' 
            AND (prev_visibility IS NULL OR prev_visibility <> 'daylight')),
        DaylightEnd AS (
        SELECT 
            timestamp AS end_time
        FROM 
            Sequences
        WHERE 
            visibility = 'daylight' 
            AND (next_visibility IS NULL OR next_visibility <> 'daylight')
        ),
        DaylightPeriods AS (
        SELECT 
            start_time, 
            (SELECT MIN(end_time) FROM DaylightEnd WHERE end_time >= start_time) AS end_time
        FROM 
            DaylightStart
        )
        SELECT * FROM DaylightPeriods ORDER BY start_time DESC LIMIT {limit};
        """
    )
    row = cursor.fetchall()
    conn.close()

    return row


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d
